package com.renseki.app.projectpertamaku

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.renseki.app.projectpertamaku.model.DataUser
import com.renseki.app.projectpertamaku.model.MainActivity
import kotlinx.android.synthetic.main.activity_welcome.*

class WelcomeActivity : AppCompatActivity() {

    companion object {
        private const val EXTRA_USER = "EXTRA_USER"

        fun getStartIntent(
                context: Context,
                DataUser: DataUser
        ) = Intent(context, WelcomeActivity::class.java).apply {
            putExtra(EXTRA_USER, DataUser)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)

        val user = intent.getParcelableExtra<DataUser>(EXTRA_USER)

        welcome.text = getString(R.string.welcome) + user.username;

    }
}
