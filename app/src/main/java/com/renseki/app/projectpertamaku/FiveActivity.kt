package com.renseki.app.projectpertamaku

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.renseki.app.projectpertamaku.model.DataUser

class FiveActivity : AppCompatActivity() , LoginInputFragment.LoginInputActionListener {

    private lateinit var errorMessageFragment: ErrorMessageFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_activity)

        fragmentLogin()
        fragmentErrorMessage()
    }

    private fun fragmentErrorMessage() {
        errorMessageFragment = ErrorMessageFragment.newInstance()

        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(
                R.id.fragment_container_2,
                errorMessageFragment
        )
        fragmentTransaction.commit()
    }

    private fun fragmentLogin() {
        val fragment = LoginInputFragment.newInstance(this)

        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(
                R.id.fragment_container_1,
                fragment
        )
        fragmentTransaction.commit()
    }

    override fun onDataUserRady(DataUser: DataUser) {
        errorMessageFragment.ReceiveErrorMessage(DataUser)
    }


}