package com.renseki.app.projectpertamaku

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.OrientationEventListener
import android.view.View
import android.view.ViewGroup
import com.renseki.app.projectpertamaku.model.DataUser
import kotlinx.android.synthetic.main.login_input_fragment.*

class LoginInputFragment : Fragment() {

    private  lateinit var listener: LoginInputActionListener

    interface LoginInputActionListener {
        fun onDataUserRady(DataUser : DataUser)
    }

    companion object {

        fun newInstance(listener: LoginInputActionListener): LoginInputFragment {
            val fragment = LoginInputFragment()
            fragment.listener = listener
            return fragment
        }

    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(
                R.layout.login_input_fragment,
                container,
                false
        )

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        btn_login.setOnClickListener {
            val username = the_real_input_username.text.toString()
            val pass = the_real_input_password.text.toString()

            sendToOtherFragment(username,pass)
        }
    }

    private fun sendToOtherFragment(username: String, pass: String) {

        val DataUser = DataUser(
                username,pass
        )
        listener.onDataUserRady(DataUser)

    }

}